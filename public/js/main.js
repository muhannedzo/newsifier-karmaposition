$(document).ready(function () {
    // new WOW().init();

    // $(".lazy").Lazy();
    const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });
    function responseError(jqXhr, textStatus, errorMessage) {
        var error = '';
        $.each(jqXhr.responseJSON.errors, function (key, value) {
            error = error + '<h6>- ' + value + '</h6>';
        });
        if (error === '') {
            error = jqXhr.responseJSON.message;
        }

        Toast.fire({
            icon: 'error',
            title: error
        });
    }





    // desktop
    var input = document.querySelector(".phone");

    $(".phone").focus(function () {
        $(this).val($(".iti__selected-dial-code").text());
    });
    if (input != null) {
        window.intlTelInput(input, {
            nationalMode: true,
            separateDialCode: true,
            preferredCountries: ['ae'],
        });
    }

    if (window.scrollY >= 20) {
        $('.navbar').addClass('dark-nav');
    } else {
        $('.navbar').removeClass('dark-nav');
    }
    $(window).scroll(function () {
        if (window.scrollY >= 20) {
            $('.navbar').addClass('dark-nav');
        } else {
            $('.navbar').removeClass('dark-nav');
        }
    });



    $(document).on('submit', '.contact-form-web', function (e)  {
        e.preventDefault();
        if ($(this).parsley()) {
            $('.send-contact-mail').html('SENDING...').attr('disabled', true);
            var name = $(this).find('#name').val();
            var email = $(this).find('#email').val();
            var phone = $(this).find('#phone').val();
            var message = $(this).find('#message').val();
            $.ajax(base_url + 'sendMail', {
                type: 'POST',  // http method
                dataType: "json",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {
                    name: name,
                    email: email,
                    phone: phone,
                    message: message
                },  // data to submit
                success: function (data, status, xhr) {
                    $('.send-contact-mail').html('SEND').removeAttr('disabled');
                    if (data['code'] == '1') {
                        Toast.fire({
                            icon: 'success',
                            title: data["message"]
                        });
                    } else {
                        Toast.fire({
                            icon: 'warning',
                            title: data["message"]
                        });
                    }
                },
                error: function (jqXhr, textStatus, errorMessage) {
                    $('.send-contact-mail').html('SEND').removeAttr('disabled');
                    responseError(jqXhr, textStatus, errorMessage)
                }
            });
        }
    });

});

