$(document).ready(function () {
    var url= window.location.href;
    var arr=url.split('/')[3];
    var arr1=url.split('/')[4];
        var table = $('.data-table').DataTable({
            processing: true,
            serverSide: true,
            sortable:false,
            aaSorting: [],
            searching: false,
            ajax: {
                url: base_url+'overallPosition/'+arr+'/'+arr1,
                data: function (d) {
                        d.search = $('input[type="search"]').val();
                }
            },
            columns: [
                {data: 'id', "render": function (data, type, row, meta) {
                    if(data == arr){
                        return '<div class="search" style="color:yellow">'+data+'</div>';
                       }else{
                            return '<div>'+data+'</div>';
                       }
                    }
                 }   ,
                {data: 'position', name: 'position'},
                {data: 'image_url', "render": function (data, type, row, meta) {
                        return '<div class="text-center"><img src="' + data + '" class="w-100"></div>';
                    }
                },
                {data: 'username', name: 'username'},
                {data: 'karma_score', name: 'karma_score'},
            ]
        });

    
});