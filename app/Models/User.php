<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;
    protected $table = 'users';
    protected $fillable = ['username', 'karma_score', 'image_id'];
    protected $appends = ['position'];

    public function image(){
        return $this->belongsTo(Image::class);
    }

    public function getPositionAttribute(){
        $position = User::where('karma_score', '>=', $this->karma_score)->where('id', '!=', $this->id)->count();
        return ++$position;
    }

}
