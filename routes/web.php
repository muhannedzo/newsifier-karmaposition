<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/optimization', function () {
    Artisan::call('optimize');
    Artisan::call('config:cache');
});
Route::get('/clear-optimization-cash', function () {
    Artisan::call('config:clear');
    Artisan::call('route:clear');
});
Route::get('/seed', function () {
    Artisan::call('db:seed --class=Databaseseeder');
});
Route::get('/migrate', function () {
    Artisan::call('migrate');
});

Route::get('/{id}/{n}', function () {
    return view('welcome');
});
Route::get('/overallPosition/{id}/{n}',[UserController::class,'getOverallPosition']);

