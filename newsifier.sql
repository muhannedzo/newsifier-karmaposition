-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2022 at 11:52 AM
-- Server version: 8.0.29
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newsifier`
--

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` bigint UNSIGNED NOT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `url`, `created_at`, `updated_at`) VALUES
(300189, 'https://via.placeholder.com/640x480.png/00bb00?text=non', '2022-05-03 10:24:47', '2022-05-03 10:24:47'),
(300190, 'https://via.placeholder.com/640x480.png/0077dd?text=consectetur', '2022-05-03 10:24:47', '2022-05-03 10:24:47'),
(300191, 'https://via.placeholder.com/640x480.png/00cc33?text=quisquam', '2022-05-03 10:24:47', '2022-05-03 10:24:47'),
(300192, 'https://via.placeholder.com/640x480.png/006600?text=ut', '2022-05-03 10:24:47', '2022-05-03 10:24:47'),
(300193, 'https://via.placeholder.com/640x480.png/002211?text=neque', '2022-05-03 10:24:47', '2022-05-03 10:24:47'),
(300194, 'https://via.placeholder.com/640x480.png/000033?text=tenetur', '2022-05-03 10:24:47', '2022-05-03 10:24:47'),
(300195, 'https://via.placeholder.com/640x480.png/00ff11?text=ipsum', '2022-05-03 10:24:47', '2022-05-03 10:24:47'),
(300196, 'https://via.placeholder.com/640x480.png/00bbaa?text=porro', '2022-05-03 10:24:47', '2022-05-03 10:24:47'),
(300197, 'https://via.placeholder.com/640x480.png/007722?text=doloribus', '2022-05-03 10:24:47', '2022-05-03 10:24:47'),
(300198, 'https://via.placeholder.com/640x480.png/003300?text=ut', '2022-05-03 10:24:47', '2022-05-03 10:24:47'),
(300199, 'https://via.placeholder.com/640x480.png/003344?text=dolores', '2022-05-04 06:40:47', '2022-05-04 06:40:47'),
(300200, 'https://via.placeholder.com/640x480.png/000011?text=quidem', '2022-05-04 06:40:47', '2022-05-04 06:40:47'),
(300201, 'https://via.placeholder.com/640x480.png/0088ff?text=et', '2022-05-04 06:40:47', '2022-05-04 06:40:47'),
(300202, 'https://via.placeholder.com/640x480.png/0044ff?text=rerum', '2022-05-04 06:40:47', '2022-05-04 06:40:47'),
(300203, 'https://via.placeholder.com/640x480.png/004488?text=sapiente', '2022-05-04 06:40:47', '2022-05-04 06:40:47'),
(300204, 'https://via.placeholder.com/640x480.png/0077ee?text=error', '2022-05-04 06:40:47', '2022-05-04 06:40:47'),
(300205, 'https://via.placeholder.com/640x480.png/00ee55?text=et', '2022-05-04 06:40:47', '2022-05-04 06:40:47'),
(300206, 'https://via.placeholder.com/640x480.png/007700?text=dolores', '2022-05-04 06:40:47', '2022-05-04 06:40:47'),
(300207, 'https://via.placeholder.com/640x480.png/00dddd?text=dicta', '2022-05-04 06:40:47', '2022-05-04 06:40:47'),
(300208, 'https://via.placeholder.com/640x480.png/003344?text=fugiat', '2022-05-04 06:40:47', '2022-05-04 06:40:47');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(2, '2022_05_01_075340_create_images_table', 1),
(3, '2022_05_01_080646_create_users_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `karma_score` int UNSIGNED NOT NULL DEFAULT '0',
  `image_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `karma_score`, `image_id`, `created_at`, `updated_at`) VALUES
(19, 'Jaylen Mitchell', 383, 300194, '2022-05-03 10:24:47', '2022-05-03 10:24:47'),
(20, 'Ms. Renee Fay', 15788, 300195, '2022-05-03 10:24:47', '2022-05-03 10:24:47'),
(21, 'Kiera Shanahan', 83069, 300189, '2022-05-03 10:24:47', '2022-05-03 10:24:47'),
(22, 'Barney Mante Sr.', 28586, 300197, '2022-05-03 10:24:47', '2022-05-03 10:24:47'),
(23, 'Anissa Kshlerin', 71606, 300197, '2022-05-03 10:24:47', '2022-05-03 10:24:47'),
(24, 'Chester Auer II', 49414, 300191, '2022-05-03 10:24:47', '2022-05-03 10:24:47'),
(25, 'Prof. Jeromy Klein MD', 67922, 300197, '2022-05-03 10:24:47', '2022-05-03 10:24:47'),
(26, 'Brandon Conn', 59530, 300190, '2022-05-03 10:24:47', '2022-05-03 10:24:47'),
(27, 'Mrs. Susan Olson', 90035, 300192, '2022-05-03 10:24:47', '2022-05-03 10:24:47'),
(28, 'Audra Kohler', 29981, 300198, '2022-05-03 10:24:47', '2022-05-03 10:24:47'),
(29, 'Bessie Lubowitz', 58451, 300192, '2022-05-04 06:40:47', '2022-05-04 06:40:47'),
(30, 'Prof. Stefanie Grimes I', 68269, 300204, '2022-05-04 06:40:47', '2022-05-04 06:40:47'),
(31, 'Reta Jast', 14096, 300203, '2022-05-04 06:40:47', '2022-05-04 06:40:47'),
(32, 'Karianne Champlin Jr.', 53544, 300189, '2022-05-04 06:40:47', '2022-05-04 06:40:47'),
(33, 'Luella Macejkovic', 41115, 300202, '2022-05-04 06:40:47', '2022-05-04 06:40:47'),
(34, 'Kristy West V', 56585, 300202, '2022-05-04 06:40:47', '2022-05-04 06:40:47'),
(35, 'Teagan Lubowitz', 47250, 300208, '2022-05-04 06:40:47', '2022-05-04 06:40:47'),
(36, 'Kellen Koepp', 23240, 300200, '2022-05-04 06:40:47', '2022-05-04 06:40:47'),
(37, 'Gerardo Cole', 10145, 300199, '2022-05-04 06:40:47', '2022-05-04 06:40:47'),
(38, 'Miguel Steuber', 5784, 300189, '2022-05-04 06:40:47', '2022-05-04 06:40:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD KEY `users_image_id_foreign` (`image_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=300209;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_image_id_foreign` FOREIGN KEY (`image_id`) REFERENCES `images` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
