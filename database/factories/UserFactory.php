<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use App\Models\User;
use App\Models\Image;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = User::class;
    protected $images = Image::class;

    public function definition()
    {
        $images = Image::all()->pluck('id')->toArray();
        return [
            'username' => $this->faker->unique()->name(),
            'karma_score' => $this->faker->numberBetween(0, 100000),
            'image_id' => $this->faker->randomElement($images),
        ];
    }
}
