<x-header/>
<section class="mt-5">
    <div class="container">
        <div class="row">
            <div class="col-12">

                <table class="table table-striped table-bordered data-table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Position</th>
                        <th>Image</th>
                        <th>Username</th>
                        <th>Karma Score</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<x-footer/>

<script src="{{asset('js/overall.js')}}"></script>

