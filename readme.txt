Hello Dears,
Steps to run the project:
1- create database in phpmyadmin.
2- open the project in vs code or whatever you want.
3- go to env file and add your db credentials.
4- in terminal run 'php artisan key:generate'
5- in terminal run 'npm install'
6- in terminal run 'composer update'
7- in terminal run 'php artisan serve'
8- go to the browser and go to link '127.0.0.1:8000/migrate' then db tables will be created and migrated.
9- go to the browser and go to link '127.0.0.1:8000/seed' then db tables will be filled with dummy data.
10- to view the simple html in browser got to link 'http://127.0.0.1:8000/id of element/number of elements to retrieve'
if id = -1 then the full overall table will be viewed, if id is exist and number of elements = -1 will view 5 elements by default
11- there a postman collection to try the api: api link (http://127.0.0.1:8000/api/v1/users/id/n) and also same notes for params.
12- i also add db export dump file if you want to just import it
Thanks and regards