<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Resources\User as UserResource;
use App\Http\Controllers\API\BaseController as BaseController;

class UserController extends Controller
{
    //
    function index(){
        return view('welcome');
    }

    public function getOverallPosition($id,$numberOfElements)
    {
        try{
            if($numberOfElements == -1){
                $numberOfElements = 5;
            }
            if($id == -1){
                $users= User::orderBy('karma_score','desc')->get();                
                return datatables()->of(UserResource::collection($users))
                ->addIndexColumn()
                ->make(true);
            }
            $user = User::orderBy('karma_score','desc')->where('id',$id)->first();
            if($user){
                $overallCount= User::orderBy('karma_score','desc')->count();
                $higherCounts = User::with('image')->where('id', '!=', $id)->where('karma_score', '>=', $user->karma_score)->orderBy('karma_score', 'DESC')->count();
                $lowerCounts = User::with('image')->where('id', '!=', $id)->where('karma_score', '<=', $user->karma_score)->orderBy('karma_score', 'DESC')->count();
                $higherLimit = 0;
                $lowerLimit = 0;
                $limit = $numberOfElements - 1;
                $lowerLimit = (int) ($limit/2); 
                $higherLimit = $limit - $lowerLimit;
                if($lowerCounts < $lowerLimit){
                    $higherLimit += $lowerLimit - $lowerCounts;
                    $lowerLimit = $lowerCounts;
                }
                if($higherCounts < $higherLimit){
                    $lowerLimit += $higherLimit - $higherCounts;
                    $higherLimit = $higherCounts;
                }
                if($user->position == 1){
                    $lowerLimit = $numberOfElements - 1;
                }
                if($user->position == $overallCount){
                    $higherLimit = $numberOfElements - 1;
                }
                if($limit == 1 && $user->position == $overallCount){
                        $higherLimit = $limit;
                        $lowerLimit = 0;
                }
                if($limit == 1 && $user->position == 1){
                        $lowerLimit = $limit;
                        $higherLimit = 0;
                }
                $lower = User::where('id', '!=', $id)
                    ->where('karma_score', '<=', $user->karma_score)
                    ->limit($lowerLimit)
                    ->orderBy('karma_score', 'DESC')
                    ->get();
                $current = User::where('id', $id)
                    ->get();
                $higher = User::where('id', '!=', $id)
                    ->where('karma_score', '>=', $user->karma_score)
                    ->limit($higherLimit)
                    ->orderBy('karma_score', 'ASC')
                    ->get();
                $merged = $higher->reverse()->merge($current);
                $merged = $merged->merge($lower);
                $result = $merged->all();
               
                return datatables()->of(UserResource::collection($result))
                    ->addIndexColumn()
                    ->make(true);
            }else{
                return $this->sendError('user not found');
            }
        }catch(Exception $e){
            return $this->sendError($e->getMessage());
        }
    }


}
